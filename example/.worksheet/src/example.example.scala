package example

object example {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(120); 
	def sum(xs: List[Int]): Int = {
		if (xs.isEmpty) 0
	  else xs.head + sum(xs.tail)
	};System.out.println("""sum: (xs: List[Int])Int""");$skip(18); val res$0 = 
	sum(List(1,2,3));System.out.println("""res0: Int = """ + $show(res$0));$skip(176); 
	
	def max(xs: List[Int]): Int = {
	    var m: Int = 0
	    if (xs.length == 1) return xs.head
	    else m = max(xs.tail)
	    if (m > xs.head) return m else return xs.head
	};System.out.println("""max: (xs: List[Int])Int""");$skip(36); val res$1 = 
	
	max(List(1,3,2,5,1,10,5,8,20,5));System.out.println("""res1: Int = """ + $show(res$1))}
}
