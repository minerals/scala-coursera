#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""Pascal's Triangle

         1
        1 1
       1 2 1
      1 3 3 1
     1 4 6 4 1
    1 5 0 0 5 1

10 == 0 for ease of read

Implement a function that returns a number from Pascal's triangle
given col and row, pascal(col, row). For example, pascal(0, 2) -> 1,
pascal(1, 2) -> 2, pascal(1, 3) -> 3

"""

rows = 10


def toprint():
    n = 0
    for i in xrange(rows):
        print ' ' * (rows - i),
        for j in xrange(i):
            n += 1
            print 0,
        print

toprint()
