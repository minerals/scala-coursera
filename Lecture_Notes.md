# Evaluation Strategies (1.2)
**Call-by-value** has the advantage that every function argument is evaluated only once. 
**Call-by-name** has the advantage that a function argument is not evaluated at all if the corresponding parameter is not used in the evaluation of the function's body. 


**Call-by-value** -- evaluate function arguments first:

  def test(x:Int, y:Int) = x * y
 
  test(2, 1+3)
  test(2, 4)
  2 * 4
  8
  same complexity

  def test(x:Int, y:Int) = x * x
  
  test(2, 1+3)
  test(2, 4)
  2 * 2
  4
  slower
  
  test(1+1, 4)
  test(2, 4)
  2 * 2
  4
  faster

**Call-by-name** -- leave argument evaluation until later:

  def test(x:Int, y:Int) = x * y

  test(2, 1+3)
  2 * (1+3)
  2 * 4
  8
  same complexity

  def test(x:Int, y:Int) = x * x
 
  test(2, 1+3)
  2 * 2
  4
  faster

  test(1+1, 4)
  (1+1) * (1+1)
  2 * (1+1)
  2 * 2
  4
  slower
