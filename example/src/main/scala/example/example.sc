package example

object example {
	def sum(xs: List[Int]): Int = {
		if (xs.isEmpty) 0
	  else xs.head + sum(xs.tail)
	}                                         //> sum: (xs: List[Int])Int
	sum(List(1,2,3))                          //> res0: Int = 6
	
	def max(xs: List[Int]): Int = {
	    var m: Int = 0
	    if (xs.length == 1) return xs.head
	    else m = max(xs.tail)
	    if (m > xs.head) return m else return xs.head
	}                                         //> max: (xs: List[Int])Int
	
	max(List(1,3,2,5,1,10,5,8,20,5))          //> res1: Int = 20
}